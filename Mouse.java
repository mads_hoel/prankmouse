import java.awt.Robot;
import java.awt.AWTException;
import java.awt.Toolkit;
import java.awt.Dimension;

public class Mouse {
    public static void main(String args[]) {

        try {
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = toolkit.getScreenSize();

            Robot robot = new Robot();
            try {
                int startX = (int) (screenSize.getWidth() / 2);
                int startY = (int) (screenSize.getHeight() / 2);
		double r = startY*0.50;
		double runningMinutes = 1440;
		int runningMilliSeconds = (int) (runningMinutes*60*1000);
		int sleepMilliSeconds = 30;
		int iterations = (int) ((double) runningMilliSeconds)/sleepMilliSeconds;

                for (int i = 0; i < iterations; i++) {
                    Thread.currentThread().sleep(sleepMilliSeconds);

		    int xCoord = (int) (startX +  r*Math.sin(2*i/100.0));
		    int yCoord = (int) (startY + r*Math.cos(2*i/100.0));

                    robot.mouseMove(xCoord, yCoord);
                }
            } catch (InterruptedException ie) {
            }
        } catch (AWTException e) {
        }
    }


}
